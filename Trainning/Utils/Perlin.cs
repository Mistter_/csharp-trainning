﻿using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Trainning.game;
using Trainning.game.Blocks;
using Trainning.game.World;

namespace Trainning.Utils
{
	public class Perlin
	{
		private float amplitude;
		private int octaves;
		private float roughness;

		private Random random = new Random();
		private int seed;
		private int xRan = 0;
		private int zRan = 0;

		public Perlin(float amplitude, int octaves, float roughness)
		{
			this.amplitude = amplitude;
			this.octaves = octaves;
			this.roughness = roughness;
			this.seed = random.Next(999999999);
			this.xRan = random.Next(1, 100000);
			this.zRan = random.Next(1, 100000);
		}

		//Quanto maior a frequência, menor a distância entre as montanhas
		public async Task<float> generateMaxHeight(int x, int z, int worldY, Vector3 chunkPosition, int chunkSize, Chunk chunk, int y)
		{
			var test = await Task.Run(() => aa(x, z, worldY, chunkPosition, chunkSize, chunk, y));
			//test.Start();
			//test.Wait();
			return test;
		}

		public float aa(int x, int z, int worldY, Vector3 chunkPosition, int chunkSize, Chunk chunk, int y)
		{
			x = x < 0 ? -x : x;
			z = z < 0 ? -z : z;

			float total = 0;
			float d = (float)Math.Pow(2, octaves - 1);
			for (int i = 0; i < octaves; i++)
			{
				float freq = (float)(Math.Pow(2, i) / d);
				float amp = (float)Math.Pow(roughness, i) * amplitude;
				total += getInterpolateNoise(x * freq, z * freq) * amp;
			}

			total = total < 0 ? -total : total;

			return (float)(int)total * 2;
		}

		public float generateStoneHeight(int x, int z)
		{
			x = x < 0 ? -x : x;
			z = z < 0 ? -z : z;

			float total = 0;
			float d = (float)Math.Pow(2, octaves - 1);
			for (int i = 0; i < octaves; i++)
			{
				float freq = (float)(Math.Pow(2, i) / d);
				float amp = (float)Math.Pow(roughness + 0.05f, i) * amplitude;
				total += getInterpolateNoise(x * freq, z * freq) * amp;
			}

			total = total < 0 ? -total : total;

			return (float)(int)(total * 2) - 5;
		}

		public float generateCaves(int x, int y, int z)
		{
			return generateCaves(x, y) + generateCaves(x, z);
		}

		public float generateCaves(int a, int y)
		{
			a = a < 0 ? -a : a;
			y = y < 0 ? -y : y;

			float total = 0;
			int oct = 6;
			float ampl = 100f;
			float roug = 0.2f;

			float d = (float)Math.Pow(2, oct - 1);
			for (int i = 0; i < oct; i++)
			{
				float freq = (float)(Math.Pow(2, i) / d);
				float amp = (float)Math.Pow(roug + 0.05f, i) * ampl;
				total += getInterpolateNoise(a * freq, y * freq) * amp;
			}

			total = total < 0 ? -total : total;

			return total;
		}

		public float generateCaves(float x, float y, float z)
		{
			int ampl = 25;
			int a = 5;
			float xin = getInterpolateNoise(y / a, z / a) * ampl;
			float yin = getInterpolateNoise(x / a, z / a) * ampl;
			float zin = getInterpolateNoise(x / a, y / a) * ampl;

			float density = xin + yin + zin;

			//Console.WriteLine(density);
			return density;
		}

		private float getInterpolateNoise(float x, float z)
		{
			int intx = (int)x;
			int intz = (int)z;
			float fracx = x - intx;
			float fracz = z - intz;

			float v1 = getSmoothNoise(intx,     intz);
			float v2 = getSmoothNoise(intx + 1, intz);
			float v3 = getSmoothNoise(intx,     intz + 1);
			float v4 = getSmoothNoise(intx + 1, intz + 1);
			float i1 = interpolate(v1, v2, fracx);
			float i2 = interpolate(v3, v4, fracx);

			return interpolate(i1, i2, fracz);
		}

		private float interpolate(float a, float b, float blend)
		{
			double theta = blend * Math.PI;
			float f = (float)(1f - Math.Cos(theta)) * 0.5f;
			return a * (1f - f) + b * f;
		}

		private float getSmoothNoise(int x, int z)
		{
			float corners = (getNoise(x - 1, z - 1) + getNoise(x + 1, z - 1) + getNoise(x - 1, z + 1)
			  + getNoise(x + 1, z + 1)) / 16f;
			float sides = (getNoise(x - 1, z) + getNoise(x + 1, z) + getNoise(x, z - 1)
					+ getNoise(x, z + 1)) / 8f;
			float center = getNoise(x, z) / 4f;
			return corners + sides + center;
		}

		//Don't put values bigger of 6 lenght
		private float getNoise(int x, int z)
		{
			Random r = new Random(x * xRan + z * zRan + seed);
			return (float) r.NextDouble() * 2f - 1f;
		}
    }
}
