﻿using OpenTK;
using System.Threading.Tasks;
using Trainning.game.Blocks;
using Trainning.Graphics;

namespace Trainning.game
{
    public class Block
    {
        public Material material;
        public bool isSolid;

        public int x;
        public int y;
        public int z;

        public Block(Material material, bool isSolid, int x, int y, int z)
        {
            this.material = material;
            this.isSolid = isSolid;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public async Task<bool> draw(Cubeside side) 
        {
            Vector3[] vertices = new Vector3[4];
            Vector2[] uvs = new Vector2[4];
            int[] triangles = new int[6];
            
            Vector2 uv00 = new Vector2(1, 1);
            Vector2 uv10 = new Vector2(0, 1);
            Vector2 uv01 = new Vector2(1, 0);
            Vector2 uv11 = new Vector2(0, 0);

            Vector3 p0 = new Vector3(-0.5f + x, -0.5f + y, 0.5f + z);
            Vector3 p1 = new Vector3(0.5f + x, -0.5f + y, 0.5f + z);
            Vector3 p2 = new Vector3(0.5f + x, -0.5f + y, -0.5f + z);
            Vector3 p3 = new Vector3(-0.5f + x, -0.5f + y, -0.5f + z);
            Vector3 p4 = new Vector3(-0.5f + x, 0.5f + y, 0.5f + z);
            Vector3 p5 = new Vector3(0.5f + x, 0.5f + y, 0.5f + z);
            Vector3 p6 = new Vector3(0.5f + x, 0.5f + y, -0.5f + z);
            Vector3 p7 = new Vector3(-0.5f + x, 0.5f + y, -0.5f + z);

            switch (side)
            {
                case Cubeside.BOTTOM:
                    vertices = new Vector3[] { p0, p1, p2, p3 };
                    uvs = new Vector2[] { uv11, uv01, uv00, uv10 };
                    triangles = new int[] { 3, 1, 0, 3, 2, 1 };
                    break;

                case Cubeside.TOP:
                    vertices = new Vector3[] { p7, p6, p5, p4 };
                    uvs = new Vector2[] { uv11, uv01, uv00, uv10 };
                    triangles = new int[] { 3, 1, 0, 3, 2, 1 };
                    break;

                case Cubeside.LEFT:
                    vertices = new Vector3[] { p7, p4, p0, p3 };
                    uvs = new Vector2[] { uv11, uv01, uv00, uv10 };
                    triangles = new int[] { 3, 1, 0, 3, 2, 1 };
                    break;

                case Cubeside.RIGHT:
                    vertices = new Vector3[] { p5, p6, p2, p1 };
                    uvs = new Vector2[] { uv11, uv01, uv00, uv10 };
                    triangles = new int[] { 3, 1, 0, 3, 2, 1 };
                    break;

                case Cubeside.FRONT:
                    vertices = new Vector3[] { p4, p5, p1, p0 };
                    uvs = new Vector2[] { uv11, uv01, uv00, uv10 };
                    triangles = new int[] { 3, 1, 0, 3, 2, 1 };
                    break;

                case Cubeside.BACK:
                    vertices = new Vector3[] { p6, p7, p3, p2 };
                    uvs = new Vector2[] { uv11, uv01, uv00, uv10 };
                    triangles = new int[] { 3, 1, 0, 3, 2, 1 };
                    break;
            }

            Vector3 a = new Vector3(uvs[0]) { Z = material.GetHashCode() };
            Vector3 b = new Vector3(uvs[1]) { Z = material.GetHashCode() };
            Vector3 c = new Vector3(uvs[2]) { Z = material.GetHashCode() };
            Vector3 d = new Vector3(uvs[3]) { Z = material.GetHashCode() };

            Vector3[] UVs = null;
            if (material == Material.GRASS && side == Cubeside.TOP)
                UVs = getUvsByNumber(material.GetHashCode() + 1);
            else if (material == Material.GRASS && side == Cubeside.BOTTOM)
                UVs = getUvsByMaterial(Material.DIRT);
            else
                UVs = new Vector3[] { a, b, c, d };

            var aba = await Task.Run(() =>
            {
                Mesh mesh = new Mesh();
                var genMesh = mesh.a(vertices, UVs, triangles);
                return genMesh;
            });
            return aba;
        }

        private static Vector3[] getUvsByMaterial(Material material)
        {
            Vector3 a = new Vector3(new Vector2(0, 0)) { Z = material.GetHashCode() };
            Vector3 b = new Vector3(new Vector2(1, 0)) { Z = material.GetHashCode() };
            Vector3 c = new Vector3(new Vector2(1, 1)) { Z = material.GetHashCode() };
            Vector3 d = new Vector3(new Vector2(0, 1)) { Z = material.GetHashCode() };

            return new Vector3[] { a, b, c, d };
        }

        private static Vector3[] getUvsByNumber(int hash)
        {
            Vector3 a = new Vector3(new Vector2(0, 0)) { Z = hash };
            Vector3 b = new Vector3(new Vector2(1, 0)) { Z = hash };
            Vector3 c = new Vector3(new Vector2(1, 1)) { Z = hash };
            Vector3 d = new Vector3(new Vector2(0, 1)) { Z = hash };

            return new Vector3[] { a, b, c, d };
        }
    }
}
