﻿using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Trainning.game.Blocks;
using Trainning.Utils;

namespace Trainning.game.World
{
    public class Chunk
    {
        public static int chunkSize = 16;

        public enum ChunkStatus { DRAW, DONE, KEEP };
        public ChunkStatus status;

        public static Dictionary<string, Chunk> chunks;

        public Block[,,] blocks;

        public Vector3 chunkPosition;

        public Chunk(World world, Vector3 chunkPosition, Perlin perlin)
        {
            this.chunkPosition = chunkPosition;
            gere(perlin);
        }

        public async Task<bool> gere(Perlin perlin)
        {
            blocks = new Block[chunkSize, chunkSize, chunkSize];

            //ThreadPool.QueueUserWorkItem(state => { 
            for (int x = 0; x < chunkSize; x++)
                for (int z = 0; z < chunkSize; z++)
                    for (int y = 0; y < chunkSize; y++)
                    {
                        int worldX = (int)(x + (chunkPosition.X * chunkSize));
                        int worldY = (int)(y + (chunkPosition.Y * chunkSize));
                        int worldZ = (int)(z + (chunkPosition.Z * chunkSize));

                        //this perlins cause a hug lag
                        var task = await perlin.generateMaxHeight(worldX, worldZ, worldY, chunkPosition, chunkSize, this, y);
                        float maxHeightY = task;

                        //float stoneHeight = perlin.generateStoneHeight(worldX, worldZ);

                        //float cave = perlin.generateCaves(worldX, worldY, worldZ);

                        /*if (cave < 7)
                            blocks[x, y, z] = new Block(Material.AIR, false, x + (int)(chunkPosition.X * chunkSize),
                                                          y + (int)(chunkPosition.Y * chunkSize), z + (int)(chunkPosition.Z * chunkSize));
                        else */
                        if (worldY == maxHeightY)
                            blocks[x, y, z] = new Block(Material.GRASS, true, x + (int)(chunkPosition.X * chunkSize),
                                                           y + (int)(chunkPosition.Y * chunkSize), z + (int)(chunkPosition.Z * chunkSize));
                        else if (worldY < maxHeightY && worldY > maxHeightY - 4)
                            blocks[x, y, z] = new Block(Material.DIRT, true, x + (int)(chunkPosition.X * chunkSize),
                                                           y + (int)(chunkPosition.Y * chunkSize), z + (int)(chunkPosition.Z * chunkSize));
                        else if (worldY < maxHeightY && worldY <= maxHeightY - 4)
                            blocks[x, y, z] = new Block(Material.STONE, true, x + (int)(chunkPosition.X * chunkSize),
                                                           y + (int)(chunkPosition.Y * chunkSize), z + (int)(chunkPosition.Z * chunkSize));
                        else
                            blocks[x, y, z] = new Block(Material.AIR, false, x + (int)(chunkPosition.X * chunkSize),
                                                          y + (int)(chunkPosition.Y * chunkSize), z + (int)(chunkPosition.Z * chunkSize));

                        status = ChunkStatus.DRAW;

                        /*
                        if (x == (chunkSize - 1) && y == (chunkSize - 1) && z == (chunkSize - 1))
                        {
                            foreach (KeyValuePair<string, Chunk> chunk in Chunk.chunks)
                            {
                                if (chunk.Value.status == Chunk.ChunkStatus.DRAW)
                                {
                                    chunk.Value.generateBlocks();
                                    chunk.Value.status = Chunk.ChunkStatus.KEEP;
                                }
                            }
                            world.interrupt = false;
                        }*/
                    }
            //});

            if (Chunk.chunks.TryGetValue(buildChunkName(chunkPosition), out Chunk value)) return false; 
            Chunk.chunks.Add(buildChunkName(chunkPosition), this);
            return false;
        }

        public async Task<bool> generateBlocks()
        {
            for (int x = 0; x < chunkSize; x++)
                for (int z = 0; z < chunkSize; z++)
                    for (int y = 0; y < chunkSize; y++)
                    {
                        if (!blocks[x, y, z].isSolid)
                            continue;

                        if (!hasSolidNeighbour(Cubeside.FRONT, x, y, (z + 1)))
                        {
                            var drawBlock = await blocks[x, y, z].draw(Cubeside.FRONT);
                            continue;
                        }
                        if (!hasSolidNeighbour(Cubeside.BACK, x, y, (z - 1)))
                        {
                            var drawBlock = await blocks[x, y, z].draw(Cubeside.BACK);
                            continue;
                        }
                        if (!hasSolidNeighbour(Cubeside.TOP, x, (y + 1), z))
                        {
                            var drawBlock = await blocks[x, y, z].draw(Cubeside.TOP);
                            continue;
                        }
                        if (!hasSolidNeighbour(Cubeside.BOTTOM, x, (y - 1), z))
                        {
                            var drawBlock = await blocks[x, y, z].draw(Cubeside.BOTTOM);
                            continue;
                        }
                        if (!hasSolidNeighbour(Cubeside.LEFT, (x - 1), y, z))
                        {
                            var drawBlock = await blocks[x, y, z].draw(Cubeside.LEFT);
                            continue;
                        }
                        if (!hasSolidNeighbour(Cubeside.RIGHT, (x + 1), y, z))
                        {
                            var drawBlock = await blocks[x, y, z].draw(Cubeside.RIGHT);
                            continue;
                        }

                        if (x + 1 == chunkSize && y + 1 == chunkSize && z + 1 == chunkSize)
                        {
                            return true;
                        }
                    }
            return true;
        }

        public bool hasSolidNeighbour(Cubeside side, int x, int y, int z)
        {
            if (x < 0 || x >= chunkSize || y < 0 || y >= chunkSize || z < 0 || z >= chunkSize)
            {
                Vector3 neighbourChunk = new Vector3(chunkPosition);

                if (side == Cubeside.BACK)
                {
                    neighbourChunk = new Vector3(chunkPosition.X, chunkPosition.Y, chunkPosition.Z - 1);
                }
                if (side == Cubeside.FRONT)
                {
                    neighbourChunk = new Vector3(chunkPosition.X, chunkPosition.Y, chunkPosition.Z + 1);
                }
                if (side == Cubeside.TOP)
                {
                    neighbourChunk = new Vector3(chunkPosition.X, chunkPosition.Y + 1, chunkPosition.Z);
                }
                if (side == Cubeside.BOTTOM)
                {
                    neighbourChunk = new Vector3(chunkPosition.X, chunkPosition.Y - 1, chunkPosition.Z);
                }
                if (side == Cubeside.LEFT)
                {
                    neighbourChunk = new Vector3(chunkPosition.X - 1, chunkPosition.Y, chunkPosition.Z);
                }
                if (side == Cubeside.RIGHT)
                {
                    neighbourChunk = new Vector3(chunkPosition.X + 1, chunkPosition.Y, chunkPosition.Z);
                }

                x = ConvertBlockIndexToLocal(x);
                y = ConvertBlockIndexToLocal(y);
                z = ConvertBlockIndexToLocal(z);

                if (Chunk.chunks.TryGetValue(buildChunkName(neighbourChunk), out Chunk chunk))
                    return chunk.blocks[x, y, z].isSolid;
                else
                    return false;
            }
            else
                return blocks[x, y, z].isSolid;
        }

        int ConvertBlockIndexToLocal(int i)
        {
            if (i == -1)
                i = chunkSize - 1;
            else if (i == chunkSize)
                i = 0;
            return i;
        }

        public static string buildChunkName(Vector3 vector)
        {
            return (int)vector.X + "_" + (int)vector.Y + "_" + (int)vector.Z;
        }
    }
}
