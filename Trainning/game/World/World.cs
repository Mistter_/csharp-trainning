﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Trainning.Graphics;
using Trainning.Utils;

namespace Trainning.game.World
{
    public class World
    {
        private int radius = 1;
        public bool interrupt;
        public bool interrupt2;

        public Perlin perlin;

        public World()
        {
            //amplitude = altura do terreno
            //octaves = quantidade de lands
            //roughtness > smooth = Rusgosidade 
            perlin = new Perlin(30f, 6, 0.2f);
            updateWorld();
        }

        public async void updateWorld()
        {
            int posx = (int)Math.Floor(PlayerController.camera.Position.X / Chunk.chunkSize);
            int posz = (int)Math.Floor(PlayerController.camera.Position.Z / Chunk.chunkSize);
            //Console.WriteLine(PlayerController.camera.Position.X + "/" + PlayerController.camera.Position.Z);

            for (int z = -radius; z < radius; z++)
                for (int x = -radius; x < radius; x++)
                    for (int y = 0; y < 2; y++)
                    {
                        Vector3 chunkPosition = new Vector3(x + posx, y, z + posz);

                        if (Chunk.chunks.TryGetValue(Chunk.buildChunkName(chunkPosition), out Chunk chunk))
                        {
                            chunk.status = Chunk.ChunkStatus.KEEP;
                            break;
                        }
                        else
                        {
                            if (!interrupt2)
                            { 
                                this.interrupt = true;
                                this.interrupt2 = true;
                                Chunk c = new Chunk(this, chunkPosition, perlin);
                                Console.WriteLine("gere");
                                var test = await c.gere(perlin);
                                this.interrupt = test;
                                this.interrupt2 = test;
                                var gen = await c.generateBlocks();
                                c.status = Chunk.ChunkStatus.KEEP;

                                /*foreach (KeyValuePair<string, Chunk> chun in Chunk.chunks)
                                {
                                    if (chun.Value.status == Chunk.ChunkStatus.DRAW)
                                    {
                                        Console.WriteLine("generate");
                                        chun.Value.generateBlocks();
                                        chun.Value.status = Chunk.ChunkStatus.KEEP;
                                    }
                                }*/
                            }
                        }
                    }


            //if (!interrupt)
            //{
                /*foreach (KeyValuePair<string, Chunk> chunk in Chunk.chunks)
                {
                    if (chunk.Value.status == Chunk.ChunkStatus.DRAW)
                    {
                        Console.WriteLine("generate");
                        await chunk.Value.generateBlocks();
                        chunk.Value.status = Chunk.ChunkStatus.KEEP;
                        continue;
                    }
                }*/
            //}
            //interrupt = false;
        }

        public void draw()
        {
            //Console.WriteLine("opaa");
            //if (interrupt) return;
            //Console.WriteLine("sad");
            foreach (KeyValuePair<int, Mesh> id in Mesh.vaoIds)
            {
                id.Value.draw();
            }
        }
    }
}
