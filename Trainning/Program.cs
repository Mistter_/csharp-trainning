﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Drawing;
using Trainning.game.Blocks;
using Trainning.game.World;
using Trainning.Graphics;
using Trainning.Utils;

namespace Trainning
{
    class Program
    {
        private static GameWindow window;
        private static string windowName = "Game launch";
        private static int width = 1280, height = 720;
        private static Matrix4 projection;
        private static Shader shader;
        public static World world;
        public static TextureArray textures;

        static void Main(string[] args)
        {
            window = new GameWindow(width, height, GraphicsMode.Default, windowName);
            window.CursorVisible = false;
            //window.TargetUpdateFrequency = 120;
            window.VSync = VSyncMode.Off;
            window.Resize += onResize;
            window.UpdateFrame += onUpdateFrame;
            window.UpdateFrame += onRenderFrame;

            shader = new Shader("shader");

            projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(60), (float)window.Width/window.Height, 0.01f, 100);

            textures = new TextureArray(16, 16, Enum.GetValues(typeof(Material)).Length + 1);
            textures.setTexture(1, new TextureData("stone.png"));
            textures.setTexture(2, new TextureData("cobblestone.png"));
            textures.setTexture(3, new TextureData("dirt.png"));
            textures.setTexture(4, new TextureData("grass_block_side.png"));
            textures.setTexture(5, new TextureData("grass_block_top.png"));
            textures.generateMipmaps();

            Chunk.chunks = new System.Collections.Generic.Dictionary<string, Chunk>();
            Mesh.vaoIds = new System.Collections.Generic.Dictionary<int, Mesh>();
            world = new World();

            window.Run();
        }

        private static void onResize(object sender, EventArgs eventArgs)
        {
            projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(60), (float)window.Width / window.Height, 0.01f, 100);
            GL.Viewport(new Point(0), window.ClientSize);
        }

        private static void onUpdateFrame(object sender, FrameEventArgs frameEventArgs)
        {
            PlayerController.update(window);
        }

        private static int fpsCounter;
        private static double fpsTimer;

        private static void onRenderFrame(object sender, FrameEventArgs frameEventArgs)
        {
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace);

            GL.ClearColor(Color4.CornflowerBlue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

            shader.Bind();
            var we = Matrix4.Identity;
            GL.UniformMatrix4(0, false, ref we);
            GL.UniformMatrix4(4, false, ref PlayerController.camera.View);
            GL.UniformMatrix4(8, false, ref projection);

            textures.Bind(TextureUnit.Texture0);

            world.draw();

            window.SwapBuffers();

            fpsCounter++;
            fpsTimer += frameEventArgs.Time;
            if (fpsTimer >= 1)
            {
                Logger.Debug("FPS: " + fpsCounter);
                fpsTimer -= 1;
                fpsCounter = 0;
            }
        }
    }
}
