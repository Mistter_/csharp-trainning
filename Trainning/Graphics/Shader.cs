﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trainning.Utils;

namespace Trainning.Graphics
{
    class Shader
    {
        public const string FragmentShaderExtension = ".fs";
        public const string VertexShaderExtExtension = ".vs";

        private readonly int programId;

        public Shader(string path)
            : this(path, File.ReadAllText(path + FragmentShaderExtension), File.ReadAllText(path + VertexShaderExtExtension))
        {
        }

        public Shader(String name, string fsSource, string vsSource)
        {
            programId = GL.CreateProgram();

            AttachShader(ShaderType.FragmentShader, fsSource);
            AttachShader(ShaderType.VertexShader, vsSource);

            GL.LinkProgram(programId);
            var infoLog = GL.GetProgramInfoLog(programId);
            if (!string.IsNullOrEmpty(infoLog))
                Logger.Error($"There was an erro linking shader \"{name}\": {infoLog}");

        }

        public void Bind() => GL.UseProgram(programId);

        public void AttachShader(ShaderType type, string source)
        {
            var id = GL.CreateShader(type);
            GL.ShaderSource(id, source);
            GL.CompileShader(id);
            GL.AttachShader(programId, id);
        }
    }
}
