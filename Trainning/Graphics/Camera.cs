﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trainning.Graphics
{
    class Camera
    {
        public Vector3 Right;
        public Vector3 Up;
        public Vector3 Forward;
        public Vector3 Position;

        public float Pitch;
        public float Yaw;

        public Matrix4 View;

        public Camera()
        {
            Right = Vector3.UnitX;
            Up = Vector3.UnitY;
            Forward = Vector3.UnitZ;
            Position = Vector3.Zero;
        }

        public void Move(Vector3 vector)
        {
            var delta = Vector3.Zero;

            delta += Right * vector.X;
            delta += Up * vector.Y;
            delta += new Vector3(Forward.X, 0, Forward.Z).Normalized() * vector.Z;

            Position += delta;
        }

        public void Rotate(float pitch, float yaw)
        {
            Pitch += pitch;
            Yaw += yaw;

            Pitch = MathHelper.Clamp(Pitch, -MathHelper.PiOver2 + 0.0001f, +MathHelper.PiOver2 - 0.0001f);
            Yaw %= MathHelper.TwoPi;
        }

        public void Update()
        {
            Forward = new Vector3((float)(Math.Sin(Yaw) * Math.Cos(Pitch)), (float)Math.Sin(Pitch), (float)(Math.Cos(Yaw) * Math.Cos(Pitch)));
            Up = Vector3.UnitY;
            Right = Vector3.Cross(Up, Forward);
            Right.NormalizeFast();

            View = Matrix4.LookAt(Position, Position - Forward, Up);
        }
    }
}
