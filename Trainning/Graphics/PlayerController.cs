﻿using OpenTK;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using Trainning.game.World;

namespace Trainning.Graphics
{
    internal static class PlayerController
    {
        public static Camera camera = new Camera();

        public static MouseState? oldState;

        public static void update(GameWindow window)
        {
            var w = Vector3.Zero;
            var ks = Keyboard.GetState();

            if (ks.IsKeyDown(Key.R))
            {
                foreach (KeyValuePair<string, Chunk> chunk in Chunk.chunks)
            {
                if (chunk.Value.status == Chunk.ChunkStatus.DRAW)
                {
                    Console.WriteLine("generate");
                    chunk.Value.generateBlocks();
                    chunk.Value.status = Chunk.ChunkStatus.KEEP;
                }
            }
            }

            if (ks.IsKeyDown(Key.A))
                w.X -= 0.1f;
            if (ks.IsKeyDown(Key.D))
                w.X += 0.1f;
            if (ks.IsKeyDown(Key.ShiftLeft))
                w.Y -= 0.2f;
            if (ks.IsKeyDown(Key.Space))
                w.Y += 0.2f;
            if (ks.IsKeyDown(Key.W))
                w.Z -= 0.1f;
            if (ks.IsKeyDown(Key.S))
                w.Z += 0.1f;

            if (w.LengthSquared > 0.0001f)
                camera.Move(w.Normalized() * 0.5f);

            if (window.Focused)
            {
                var mouse = Mouse.GetState();
                if (oldState != null)
                {
                    var pitch = mouse.Y - oldState.Value.Y;
                    var yaw = oldState.Value.X - mouse.X;
                    camera.Rotate(pitch * 0.008f, yaw * 0.008f);
                }
                oldState = mouse;
                Mouse.SetPosition(window.X + window.Width / 2, window.Y + window.Height / 2);
            }

            camera.Update();
            Program.world.updateWorld();
        }
    }
}
