﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Trainning.Graphics
{
    class Mesh
    {
        public static Dictionary<int, Mesh> vaoIds;

        private readonly int vao;
        private readonly int[] bufferIds = new int[2];
        private readonly int index;
        private int count;

        public Mesh()
        {
            vao = GL.GenVertexArray();
            Console.WriteLine(vao);
            GL.GenBuffers(bufferIds.Length, bufferIds);
            index = GL.GenBuffer();

            GL.BindVertexArray(vao);
        }

        public async Task<bool> a(Vector3[] vertices, Vector3[] uvs, int[] triangles)
        {
            //if (vaoIds.TryGetValue(vao, out Mesh value)) return;
            count = triangles.Length;

            GL.BindBuffer(BufferTarget.ArrayBuffer, bufferIds[0]);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * Vector3.SizeInBytes, vertices.ToArray(), BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.EnableVertexArrayAttrib(vao, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, bufferIds[1]);
            GL.BufferData(BufferTarget.ArrayBuffer, uvs.Length * Vector3.SizeInBytes, uvs.ToArray(), BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.EnableVertexArrayAttrib(vao, 1);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, index);
            GL.BufferData(BufferTarget.ElementArrayBuffer, triangles.Length * sizeof(uint), triangles.ToArray(), BufferUsageHint.StaticDraw);
            //Console.WriteLine(vao);
            vaoIds.Add(vao, this);
            return true;
        }

        public void draw()
        {
            GL.BindVertexArray(vao);
            GL.DrawElements(BeginMode.Triangles, count, DrawElementsType.UnsignedInt, 0);
        }

        public void Dispose()
        {
            GL.DeleteBuffer(index);
            GL.DeleteBuffers(bufferIds.Length, bufferIds);
            GL.DeleteVertexArray(vao);
        }
    }
}
