﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Trainning.Graphics
{
    internal class TextureData : IDisposable
    {
        private readonly Bitmap bpm;
        private readonly bool ownsBmp;

        public readonly BitmapData Data;
        public int Width => Data.Width;
        public int Height => Data.Height;
        public IntPtr DataPtr => Data.Scan0;

        public TextureData(string filename): this((Bitmap)Image.FromFile(filename))
        {
            ownsBmp = true;
        }

        private TextureData(Bitmap bitmap)
        {
            bpm = bitmap;
            ownsBmp = false;

            Data = bitmap.LockBits(new Rectangle(new Point(0), bitmap.Size), ImageLockMode.ReadOnly,
                PixelFormat.Format32bppArgb);
        }

        public void Dispose()
        {
            bpm.UnlockBits(Data);
            if (ownsBmp) bpm.Dispose();
        }
    }
}
